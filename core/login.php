<?php
include 'core/database.php';
include 'core/session.php';
include 'core/permission.php';
?>
<!doctype html>
<html>
<head>
  <title>Connexion</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <?php
  include('template/header.php');
  if ($logged==1) {
    header('Location:index.php');
  }
  ?>
  <div class="container">
    <?php

    if(isset($_POST["submit"])){
      $email=htmlentities(addslashes($_POST["email"]));
      $password=$_POST["password"];
      if($username&&$password) {
        $password=sha1($password);
        $query="SELECT * FROM user WHERE email='$email'&&password='$password'";
        $result = mysqli_query($handle,$query);
        if($handle->affected_rows > 0) {
          $line=mysqli_fetch_array($result);
          $_SESSION['email']=$line['email'];
          $_SESSION['id']=$line['id'];
          header('Location:index.php');

        } else {
          echo "Identifiant ou mot de passe incorecte";
        }

      } else {
        echo "Veuillez saisir tous les champs";
      }
    }

    ?>
    <h3>Connexion</h3>

    <form method="POST" action="login.php">
      <div class="form-group">
        <label for="email">E-mail</label>
        <input type="text" class="form-control" tabindex="1" name="email" placeholder="Identifiants de connexion">
      </div>
      <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" class="form-control" tabindex="2" name="password" placeholder="Mot de passe">
      </div>
      <button type="submit" tabindex="3" name ="submit" class="btn btn-info">GO</button>
    </form>
  </div>


  <?php include ('template/footer.php'); ?>
